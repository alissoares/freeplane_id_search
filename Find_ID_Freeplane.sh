#!/bin/bash
# a simple shellscript script that finds the IF of nodes in all yout mindmap folder
# It can also be used to find an information in all your mindmaps

local="/home/user/MindMaps_Freemind" # Change this location to use this function
echo "Search ID in all mindmaps"
TERMO=$1
buscado=$(grep -Eir "$TERMO" "$local" | sed -E 's/.*node TEXT=(\".*\") .* ID=\"(ID_[0-9]+)\" .*/\1 \2/g' | sed -E 's/(\".*\") .* (ID_.*)/\1 \2/g')
while read i; do 
    NOME_ARQ=$(echo "$i" | awk -F":" '{print $1}')
    No=$(echo "$i" | grep -Eo "\".*\"")
    ID=$(echo "$i" | grep -Eo "ID_[0-9]+")
    echo "$NOME_ARQ#$ID $No"
done < <(echo "$buscado") | grep -Ez '[A-Za-z0-9\\-\_ ]+\.mm#ID_[0-9]+'
