# freeplane_id_search

Search in freeplane/freemind directory in all mindmaps, returning it's name and ID 

If you 
- use mindmaps in freeplane or freemind, (so, all your mindmaps are located in one specific folder). 
- use Linux
- want to search in all your mindmaps for a specific information, getting the filename as well as it's ID (good to do croossreference between different mindmaps)

Just copy and paste this function, altering the address of your mindmaps.